use std::env;
use std::fs;
use std::sync::mpsc::{channel, Sender};
use std::sync::Mutex;
use anyhow::Result;
use dotenv::dotenv;
use megalodon::entities::{Account, Status, StatusVisibility};
use megalodon::entities::notification::NotificationType;
use megalodon::{Megalodon, Streaming};
use megalodon::streaming::Message;
use futures::executor::block_on;

type Client = Box<dyn Megalodon + Send + Sync>;

const HQLEAKS_TRIED_TO_FOLLOW: &str = "hqleaks::tried_to_follow";

async fn synchronize_followers(client: &Client, own_account: &Account) -> Result<()> {
    log::info!("Synchronizing followers...");

    let followers = client.get_account_followers(own_account.id.clone(), None).await?.json;
    let relationships = client.get_relationships(
        followers.iter().map(|follower| follower.id.clone()).collect()
    ).await?.json;

    log::info!("Fetched {} follower(s) and our relationship(s) to them, checking if we already follow or requested...", followers.len());

    for relationship in relationships {
        let account = client.get_account(relationship.id.clone()).await?.json;
        let (relationship, account) = if account.moved.is_some() {
            let new_account = account.moved.unwrap();
            log::debug!("{} ({}) moved to {} ({}), using new account...", account.acct, account.id, new_account.acct, new_account.id);

            (client.get_relationships(vec![new_account.id.clone()]).await?.json.first().unwrap().to_owned(), new_account)
        } else { (relationship, Box::new(account)) };

        if relationship.id == own_account.id {
            log::debug!("Cannot follow self");
            continue;
        }

        if relationship.following || relationship.requested {
            log::debug!("Already following or requested {} ({})...", account.acct, relationship.id);
            continue;
        }

        if relationship.note == Some(HQLEAKS_TRIED_TO_FOLLOW.to_string()) {
            log::debug!("Note with {HQLEAKS_TRIED_TO_FOLLOW} exists and not following, assuming rejected. {} ({})", account.acct, relationship.id);
            continue;
        }

        log::debug!("Creating follow for {} ({})...", account.acct, relationship.id);
        client.follow_account(relationship.id, None).await?;
        client.set_account_note(account.id.clone(), Some(HQLEAKS_TRIED_TO_FOLLOW.to_string())).await?;
    }

    log::debug!("Done synchronizing followers.");
    Ok(())
}

async fn handle_message(client: &Client, own_account: &Account, message: Message, seen_posts: &mut Vec<String>, tags_to_boost: &Vec<String>, follow_accounts_back: bool, boost_boosts: bool, boost_when_tagged: bool) -> Result<()> {
    match message {
        Message::Update(status) |
        Message::StatusUpdate(status) => {
            if seen_posts.contains(&status.id) {
                log::debug!("Ignoring post {}, it has been seen already", status.id);
                return Ok(());
            }

            if status.tags.iter().any(|tag| tags_to_boost.contains(&tag.name)) {
                handle_boost(client, &status, seen_posts, boost_boosts).await?;
            }

            if boost_when_tagged && status.in_reply_to_id.is_none() && status.mentions.iter().any(|mention| mention.id == own_account.id) {
                handle_boost(client, &status, seen_posts, boost_boosts).await?;
            }
        }
        Message::Notification(notification) => {
            if !follow_accounts_back {
                return Ok(());
            }

            if let NotificationType::Follow = notification.r#type {
                let Some(account) = notification.account else {
                    log::error!("Follow notification did not have account.");
                    return Ok(());
                };

                log::info!("Got follow from @{}, following back", account.acct);

                client.follow_account(account.id.clone(), None).await?;
                client.set_account_note(account.id.clone(), Some(HQLEAKS_TRIED_TO_FOLLOW.to_string())).await?;
            }
        }
        _ => {}
    }

    Ok(())
}

async fn handle_boost(client: &Client, status: &Status, seen_posts: &mut Vec<String>, boost_boosts: bool) -> Result<()> {
    match status.visibility {
        StatusVisibility::Public |
        StatusVisibility::Unlisted => {
            if !boost_boosts && status.reblog.is_some() {
                return Ok(());
            }

            log::info!("Reblogging {}", status.url.to_owned().unwrap_or_else(|| "None".to_string()));

            seen_posts.push(status.id.clone());
            client.reblog_status(status.id.clone()).await?;
        }
        _ => {}
    }

    Ok(())
}

fn spawn_streaming_consumer(streaming: Box<dyn Streaming + Send + Sync>, sender: &Sender<Message>) {
    let sender = sender.clone();

    tokio::task::spawn_blocking(move || {
        let sender = Mutex::new(sender);

        block_on(streaming.listen(Box::new(move |message| sender.lock().unwrap().send(message).unwrap())))
    });
}

#[tokio::main]
async fn main() -> Result<()> {
    dotenv().ok();
    env_logger::init_from_env(env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"));

    log::info!("Starting up");

    let base_url = env::var("MASTODON_BASE_URL").expect("MASTODON_BASE_URL should be set");
    let token = env::var("MASTODON_ACCESS_TOKEN").ok()
        .or_else(|| env::var("MASTODON_ACCESS_TOKEN_FILE").ok().and_then(|path| fs::read_to_string(path).ok()))
        .expect("MASTODON_ACCESS_TOKEN or MASTODON_ACCESS_TOKEN_FILE should be set and in case of a file the read must succeed")
        .trim().to_owned();
    let tags: Vec<String> = env::var("TAGS").expect("TAGS should be set").split(',').map(|tag| tag.trim_start_matches('#').to_string()).collect();
    let follow_tag: bool = env::var("FOLLOW_TAG").expect("FOLLOW_TAG should be set").parse().expect("FOLLOW_TAG should be a bool");
    let follow_accounts_back: bool = env::var("FOLLOW_ACCOUNTS_BACK").expect("FOLLOW_ACCOUNTS_BACK should be set").parse().expect("FOLLOW_ACCOUNTS_BACK should be a bool");
    let boost_boosts: bool = env::var("BOOST_BOOSTS").expect("BOOST_BOOSTS should be set").parse().expect("BOOST_BOOSTS should be a bool");
    let boost_when_tagged: bool = env::var("BOOST_WHEN_TAGGED").expect("BOOST_WHEN_TAGGED should be set").parse().expect("BOOST_WHEN_TAGGED should be a bool");

    log::info!("Boosting posts tagged with {}", tags.iter().map(|tag| format!("#{}", tag)).collect::<Vec<_>>().join(", "));

    if boost_boosts {
        log::info!("Boosting boosts {boost_boosts}");
    }

    let client = megalodon::generator(
        megalodon::SNS::Mastodon,
        base_url,
        Some(token),
        None,
    );

    let account = client.verify_account_credentials().await?.json;

    if follow_accounts_back {
        log::info!("Following accounts back automatically");
        synchronize_followers(&client, &account).await?;
    }

    for tag in &tags {
        if follow_tag {
            log::info!("Following tag #{tag}");
            client.follow_tag(tag.clone()).await?;
        } else {
            log::info!("Not following tag #{tag}");
            client.unfollow_tag(tag.clone()).await?;
        }
    }

    let (sender, receiver) = channel();

    spawn_streaming_consumer(client.user_streaming().await, &sender);

    if follow_tag {
        for tag in &tags {
            spawn_streaming_consumer(client.tag_streaming(tag.clone()).await, &sender);
        }
    }

    let mut seen_posts = Vec::new();

    log::info!("Finished setup phase, handling incoming messages");

    for message in receiver {
        handle_message(&client, &account, message, &mut seen_posts, &tags, follow_accounts_back, boost_boosts, boost_when_tagged).await?;
    }

    Ok(())
}
