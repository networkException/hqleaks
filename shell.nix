let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  buildInputs = with pkgs; [ cargo clippy rustc pkg-config openssl.dev ];
}
