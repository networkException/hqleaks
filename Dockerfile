# NOTE: This image is not alpine based as additional build dependencies would be required.
FROM rust:bookworm AS build

WORKDIR /usr/src/

RUN USER=root cargo new hqleaks
WORKDIR /usr/src/hqleaks

COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

COPY src ./src
RUN cargo install --path .

FROM debian:bookworm

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update -y && apt install -y --no-install-recommends ca-certificates

COPY --from=build /usr/local/cargo/bin/hqleaks .

USER 1000

CMD ["./hqleaks"]
